
var player;

function onYouTubePlayerAPIReady() {
  player = new YT.Player('video', {
    events: {
      onReady: onPlayerReady,
    },
  });
}

function onPlayerReady(event) {
  updateTimerDisplay();
  updateProgressBar();

  var playButton = document.getElementById('play-button');
  playButton.addEventListener('click', function () {
    player.playVideo();
  });

  var pauseButton = document.getElementById('pause-button');
  pauseButton.addEventListener('click', function () {
    player.pauseVideo();
  });

  var getTimeButton = document.getElementById('get-time');
  getTimeButton.addEventListener('click', function () {
    var time = player.getCurrentTime();
    console.log(formatTime(time));
  });
}

function updateTimerDisplay() {
  $('#current-time').text(formatTime(player.getCurrentTime()));
  $('#duration').text(formatTime(player.getDuration()));
}

function formatTime(time) {
  return moment(0)
    .add(moment.duration({ seconds: time }))
    .format('mm:ss');
}

function updateProgressBar() {
  $('#progress-bar').val((player.getCurrentTime() / player.getDuration()) * 100);
}

function getTime() {
  console.log(player.getDuration());
}

var tag = document.createElement('script');
tag.src = '//www.youtube.com/player_api';
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
