// Auth

export interface IloginPayload {
  email: string;
  password: string;
}

export interface IloginAction {
  type: string;
  payload: IloginPayload;
}

export interface IregisterPayload {
  name: string;
  email: string;
  password: string;
}

export interface IregisterAction {
  type: string;
  payload: IregisterPayload;
}

export interface IlogoutAction {
  type: string;
}

export interface IgetProgileAction {
  type: string;
}

export interface IupdateProfilePayload {
  name: string;
  birthday: string;
  avatar: string;
  sex: number;
}

export interface IupdateProfileAction {
  type: string;
  payload: IupdateProfilePayload;
}

export interface IgetMyClassAction {
  type: string;
}

// Course

export interface IGetCoursePayload {
  id: string;
}

export interface IgetCourseAction {
  type: string;
  payload: IGetCoursePayload;
}

export interface IgetLessonPayload {
  id_course: string;
  id_lesson: string;
}

export interface IgetLessonAction {
  type: string;
  payload: IgetLessonPayload;
}
