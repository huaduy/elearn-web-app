// Course

import {
  IgetCourseAction,
  IGetCoursePayload,
  IgetLessonAction,
  IgetLessonPayload,
} from './interface';

export const GET_COURSE_REQUEST = 'GET_COURSE_REQUEST';
export const GET_COURSE_SUCCESS = 'GET_COURSE_SUCCESS';
export const GET_COURSE_FAILD = 'GET_COURSE_FAILD';

export const GET_LESSON_REQUEST = 'GET_LESSON_REQUEST';
export const GET_LESSON_SUCCESS = 'GET_LESSON_SUCCESS';
export const GET_LESSON_FAILD = 'GET_LESSON_FAILD';

export const getCourseActions = (payload: IGetCoursePayload): IgetCourseAction => {
  return {
    type: GET_COURSE_REQUEST,
    payload: payload,
  };
};

// lesson

export const getLessonActions = (payload: IgetLessonPayload): IgetLessonAction => {
  return {
    type: GET_LESSON_REQUEST,
    payload: payload,
  };
};
