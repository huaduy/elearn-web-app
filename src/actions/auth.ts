import {
  IgetMyClassAction,
  IgetProgileAction,
  IloginAction,
  IloginPayload,
  IlogoutAction,
  IregisterAction,
  IregisterPayload,
  IupdateProfileAction,
  IupdateProfilePayload,
} from './interface';

export const LOGIN_REQUEST = 'LOGIN_REQUEST';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAIL = 'LOGIN_FAIL';

export const REGISTER_REQUEST = 'REGISTER_REQUEST';
export const REGISTER_SUCCESS = 'REGISTER_SUCCESS';
export const REGISTER_FAIL = 'REGISTER_FAIL';

export const LOGOUT_REQUEST = 'LOGOUT_REQUEST';

export const GET_PROFILE = 'GET_PROFILE';
export const GET_PROFILE_SUCCESS = 'GET_PROFILE_SUCCESS';
export const GET_PROFILE_FAIL = 'GET_PROFILE_FAIL';

export const UPDATE_PROFILE_REQUEST = 'UPDATE_PROFILE_REQUEST';
export const UPDATE_PROFILE_SUCCESS = 'UPDATE_PROFILE_SUCCESS';
export const UPDATE_PROFILE_FAIL = 'UPDATE_PROFILE_FAIL';

export const GET_MY_CLASS_REQUEST = 'GET_MY_CLASS_REQUEST';
export const GET_MY_CLASS_SUCCESS = 'GET_MY_CLASS_SUCCESS';
export const GET_MY_CLASS_FAIL = 'GET_MY_CLASS_FAIL';

// Auth

export const loginAction = (payload: IloginPayload): IloginAction => {
  return {
    type: LOGIN_REQUEST,
    payload: payload,
  };
};

export const registerAction = (payload: IregisterPayload): IregisterAction => {
  return {
    type: REGISTER_REQUEST,
    payload: payload,
  };
};

export const logoutAction = (): IlogoutAction => {
  return {
    type: LOGOUT_REQUEST,
  };
};

// Personal

//  **** Profile *****

export const getProfileAction = (): IgetProgileAction => {
  return {
    type: GET_PROFILE,
  };
};

export const updateProfileAction = (payload: IupdateProfilePayload): IupdateProfileAction => {
  return {
    type: UPDATE_PROFILE_REQUEST,
    payload: payload,
  };
};

// ***** My class *****

export const getMyClassAction = (): IgetMyClassAction => {
  return {
    type: GET_MY_CLASS_REQUEST,
  };
};
