import {
  GET_PROFILE_SUCCESS,
  LOGIN_FAIL,
  LOGIN_SUCCESS,
  LOGOUT_REQUEST,
  REGISTER_FAIL,
  REGISTER_SUCCESS,
  GET_MY_CLASS_SUCCESS,
  GET_MY_CLASS_FAIL,
} from '../actions/auth';

const initialState = {
  isLogin: false,
  profile: undefined,
  register: false,
  message: '',
  myClass: [],
};

const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOGIN_SUCCESS: {
      return {
        ...state,
        isLogin: true,
        profile: action.payload?.data?.infoUser,
        message: '',
      };
    }
    case LOGOUT_REQUEST:
      return {
        profile: undefined,
        message: '',
      };
    case REGISTER_SUCCESS: {
      return {
        isLogin: false,
        register: true,
        message: '',
      };
    }
    case GET_PROFILE_SUCCESS: {
      return {
        profile: action.payload,
      };
    }
    case GET_MY_CLASS_SUCCESS: {
      return {
        ...state,
        myClass: action.payload,
      };
    }
    case GET_MY_CLASS_FAIL: {
      return {
        ...state,
        myClass: [],
      };
    }
    case REGISTER_FAIL: {
      return {
        ...state,
        isLogin: false,
        register: false,
        message: action.payload,
      };
    }
    case LOGIN_FAIL:
      return {
        ...state,
        isLogin: false,
        message: action.payload,
      };
    default:
      return {
        ...state,
      };
  }
};

export default authReducer;
