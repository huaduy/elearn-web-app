import { GET_COURSE_FAILD, GET_COURSE_SUCCESS, GET_LESSON_SUCCESS } from 'actions/course';

const initialState = {
  course: {},
  lessons: [],
};

const courseReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_COURSE_SUCCESS: {
      return {
        ...state,
        course: action.payload,
      };
    }
    case GET_LESSON_SUCCESS: {
      const { data } = action.payload;
      const { lessons } = data;
      return {
        ...state,
        lessons: lessons,
      };
    }
    case GET_COURSE_FAILD: {
      return {
        ...state,
        course: [],
      };
    }
    default:
      return {
        ...state,
      };
  }
};

export default courseReducer;
