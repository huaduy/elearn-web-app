import { combineReducers } from 'redux';
import authReducer from '../reducer/auth';
import courseReducer from '../reducer/course';

const rootReducer = combineReducers({
  auth: authReducer,
  course: courseReducer,
});

export default rootReducer;
