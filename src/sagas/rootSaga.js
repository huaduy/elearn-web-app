import { all } from 'redux-saga/effects';
import loginSaga from './models/auth';
import courseSaga from './models/course';

export default function* mainSaga() {
  yield all([loginSaga(), courseSaga()]);
}
