import { call, put, takeEvery } from '@redux-saga/core/effects';
import { getCourseApi, getLessonApi } from 'api/course';
import {
  GET_COURSE_SUCCESS,
  GET_COURSE_FAILD,
  GET_COURSE_REQUEST,
  GET_LESSON_SUCCESS,
  GET_LESSON_FAILD,
  GET_LESSON_REQUEST,
} from '../../actions/course';

function* getCourse({ payload }) {
  try {
    let { data } = yield call(getCourseApi, payload);
    yield put({ type: GET_COURSE_SUCCESS, payload: data });
  } catch (error) {
    yield put({ type: GET_COURSE_FAILD, payload: error.response?.response?.data?.error });
  }
}

function* getLesson({ payload }) {
  try {
    let { data } = yield call(getLessonApi, payload);
    yield put({ type: GET_LESSON_SUCCESS, payload: data });
  } catch (error) {
    yield put({ type: GET_LESSON_FAILD, payload: error.response?.response?.data?.error });
  }
}

export default function* courseSaga() {
  yield takeEvery(GET_COURSE_REQUEST, getCourse);
  yield takeEvery(GET_LESSON_REQUEST, getLesson);
}
