import { call, put, takeEvery } from '@redux-saga/core/effects';
import { GET_COURSE_FAILD } from 'actions/course';
import {
  LOGIN_FAIL,
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  GET_PROFILE,
  GET_PROFILE_FAIL,
  GET_PROFILE_SUCCESS,
  REGISTER_REQUEST,
  REGISTER_FAIL,
  REGISTER_SUCCESS,
  GET_MY_CLASS_SUCCESS,
  GET_MY_CLASS_REQUEST,
} from '../../actions/auth';
import { getMyclassApi, getProfileApi, loginApi, registerApi } from '../../api/auth';

function* login({ payload }) {
  try {
    let { data } = yield call(loginApi, payload);
    localStorage.setItem('token', data.data.token);
    yield put({ type: LOGIN_SUCCESS, payload: data });
  } catch (error) {
    yield put({ type: LOGIN_FAIL, payload: error.response?.response?.data?.error });
  }
}

function* register({ payload }) {
  try {
    let { data } = yield call(registerApi, payload);
    localStorage.setItem('token', data.token);
    yield put({ type: REGISTER_SUCCESS, payload: data });
  } catch (error) {
    yield put({ type: REGISTER_FAIL, payload: error.response?.response?.data?.error });
  }
}

function* getProfile() {
  try {
    let { data } = yield call(getProfileApi);
    yield put({ type: GET_PROFILE_SUCCESS, payload: data });
  } catch (error) {
    yield put({ type: GET_PROFILE_FAIL, payload: error.response?.response?.data?.error });
  }
}

function* getMyClass() {
  try {
    let { data } = yield call(getMyclassApi);
    yield put({ type: GET_MY_CLASS_SUCCESS, payload: data.data });
  } catch (error) {
    yield put({ type: GET_COURSE_FAILD, payload: error.response?.response?.data?.error });
  }
}

export default function* loginSaga() {
  yield takeEvery(LOGIN_REQUEST, login);
  yield takeEvery(GET_PROFILE, getProfile);
  yield takeEvery(REGISTER_REQUEST, register);
  yield takeEvery(GET_MY_CLASS_REQUEST, getMyClass);
}
