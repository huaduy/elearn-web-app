import { Provider } from 'react-redux';
import Page from './pages';
import { PersistGate } from 'redux-persist/integration/react';
import store, { persistor } from 'sagas/store';
const App = () => {
  return (
    <Provider store={store}>
      <PersistGate persistor={persistor}>
        <Page />
      </PersistGate>
    </Provider>
  );
};
export default App;
