import React from 'react';
import './style.scss';
import { verifyEmailApi } from '../../api/auth';
import { useHistory, useParams } from 'react-router';
import { Button } from 'antd';
import { useSelector } from 'react-redux';

const VerifyEmail = () => {
  let id = useParams();
  const history = useHistory();
  const { register }: any = useSelector<any>((state) => state.auth);
  const backToHome = () => {
    history.push('/');
  };
  if (!register) {
    backToHome();
  }
  try {
    verifyEmailApi(id).then();
  } catch (error) {
    console.log(error);
  }
  try {
  } catch (error) {}
  return (
    <div className="verify-email-page">
      <div className="content">
        <div className="text-content">
          <div>
            Cảm ơn bạn đã sửa dụng dịch vụ của Elarning, Tài khoản của bạn đã được kích hoạt thành
            công
          </div>
          <div>Bấm vào nút dưới đây để quay về trang chủ</div>
        </div>
        <div className="btn-back-to-hone">
          <Button
            onClick={() => {
              backToHome();
            }}
          >
            Về Trang Chủ
          </Button>
        </div>
      </div>
    </div>
  );
};

export default React.memo(VerifyEmail);
