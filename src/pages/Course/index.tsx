import React from 'react';
import './style.scss';
import { Tabs } from 'antd';
import ContentCourse from './ContentCourse';
import { useDispatch, useSelector } from 'react-redux';
import { getCourseActions } from 'actions/course';
import { useParams } from 'react-router';

const { TabPane } = Tabs;

const Course = () => {
  const { course }: any = useSelector<any>((state) => state.course);
  let id: any = useParams();
  const dispatch = useDispatch();
  React.useEffect(() => {
    dispatch(
      getCourseActions({
        id: `${id.id}`,
      })
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div className="main-course">
      <h3 className="title-comm">
        <span className="title-holder">{course?.data?.name}</span>
      </h3>
      <div className="tabs">
        <Tabs size="large" defaultActiveKey="1" tabPosition="right">
          {course?.data?.lessons?.map((item, index) => {
            return (
              <TabPane tab={item?.name} key={index + 1}>
                <div className="content">
                  <ContentCourse lesson={item} courseId={id.id} />
                </div>
              </TabPane>
            );
          })}
        </Tabs>
      </div>
    </div>
  );
};

export default React.memo(Course);
