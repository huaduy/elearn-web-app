import { Tabs } from 'antd';
import React from 'react';
import './style.scss';
import ReactPlayer from 'react-player';
import { useDispatch, useSelector } from 'react-redux';
import { getLessonActions } from 'actions/course';
import Communicate from 'pages/Communicate';
import { IcontentCourseProps } from '../interface';

const { TabPane } = Tabs;

const ContentCourse = ({ lesson, courseId }: IcontentCourseProps) => {
  const [hideEx, setHideEx] = React.useState<boolean>(true);
  const { lessons }: any = useSelector<any>((state) => state.course);

  const onEndVideo = () => {
    setHideEx(false);
  };

  const dispatch = useDispatch();

  React.useEffect(() => {
    dispatch(
      getLessonActions({
        id_course: courseId,
        id_lesson: lesson._id,
      })
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div className="content-course">
      <ReactPlayer controls url={lessons?.linkVideo} onEnded={() => onEndVideo()} />
      <h2 className="course-title">{lessons?.name}</h2>
      <Tabs defaultActiveKey="1" size="large">
        <TabPane tab="Nội dung bài học" key="1">
          {lessons?.description}
        </TabPane>
        <TabPane tab="Bài tập" key="2" disabled={hideEx}>
          <Communicate exercises={lessons.exercises} courseId={courseId} />
        </TabPane>
        <TabPane tab="Đánh giá khóa học" key="3"></TabPane>
      </Tabs>
    </div>
  );
};

export default React.memo(ContentCourse);
