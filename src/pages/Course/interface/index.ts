export interface IcontentCourseProps {
  lesson: any;
  courseId: string;
}

export interface IcommunicateProps {
  exercises: any;
  courseId: string;
}
