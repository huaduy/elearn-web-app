import React from 'react';
import './style.scss';
import { Button, Table } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { getMyClassAction } from 'actions/auth';
import { useHistory } from 'react-router';

const MyClass = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  React.useEffect(() => {
    dispatch(getMyClassAction());
    //eslint-disable-next-line
  }, []);

  const pushToLearnPage = (id: string) => {
    history.push(`/learn/${id}`);
  };

  const { myClass }: any = useSelector<any>((state) => state.auth);

  const columns = [
    {
      title: 'Tên khóa học',
      dataIndex: 'name',
      width: '500px',
      render: (item) => {
        return <p className="class-name">{item}</p>;
      },
    },
    {
      title: 'Hành động',
      width: '300px',
      render: (item) => {
        return (
          <Button className="btn-join" onClick={() => pushToLearnPage(item._id)}>
            Vào lớp
          </Button>
        );
      },
    },
  ];

  return (
    <div className="my-class">
      <Table columns={columns} pagination={false} dataSource={myClass} />
    </div>
  );
};

export default React.memo(MyClass);
