import { AudioOutlined } from '@ant-design/icons';
import { Button } from 'antd';
import { IcommunicateProps } from 'pages/Course/interface';
import React from 'react';
import AudioHandle from './AudioHandle';

const Communicate = ({ exercises, courseId }: IcommunicateProps) => {
  // Tạo state quản lý progress
  const [btnNext, setbtnNext] = React.useState<boolean>(false);
  const [btnPre, setbtnPre] = React.useState<boolean>(true);
  // Tạo state quản lý visible của Mic
  const [visibleMic, setVisibleMic] = React.useState<boolean>(false);
  // Tạo state quản lý bài tập
  const [indexEx, setIndexEx] = React.useState<number>(0);
  const [ex, setEx] = React.useState<any>();

  const handleEx = (index) => {
    setEx(exercises[index]);
  };

  React.useEffect(() => {
    handleEx(indexEx);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [indexEx]);

  // Function show mic
  const useMic = () => {
    setVisibleMic(true);
  };

  // Function hide mic
  const cancelMic = () => {
    setVisibleMic(false);
  };

  // Function Next bài

  const nextEx = () => {
    setIndexEx(indexEx + 1);
    setbtnPre(false);
    if (indexEx === exercises.length - 2) {
      setbtnNext(true);
    }
  };

  // Function Pre bài

  const preEx = () => {
    setbtnNext(false);
    setIndexEx(indexEx - 1);
    if (indexEx === 1) {
      setbtnPre(true);
    }
  };

  return (
    <div>
      <div>
        Click the button to use mic to select answer &#160;
        <Button onClick={useMic}>
          <AudioOutlined />
        </Button>
      </div>
      <AudioHandle
        exercise={ex}
        visibleMic={visibleMic}
        cancleMic={cancelMic}
        courseId={courseId}
      />
      <div>
        <Button onClick={() => preEx()} disabled={btnPre}>
          Bài Trước
        </Button>{' '}
        <Button onClick={() => nextEx()} disabled={btnNext}>
          Bài Sau
        </Button>
      </div>
    </div>
  );
};

export default React.memo(Communicate);
