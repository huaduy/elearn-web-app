import React from 'react';
import { SpeechProvider, useSpeechContext } from '@speechly/react-client';
import { Button, Form, message, Radio, Space, Typography } from 'antd';
import {
  PushToTalkButton,
  PushToTalkButtonContainer,
  BigTranscript,
  BigTranscriptContainer,
  ErrorPanel,
} from '@speechly/react-ui';
import { IAudioHandleProps } from '../interface';
import { checkAnswerApi } from 'api/course';
import { CustomerServiceOutlined } from '@ant-design/icons';

// Component view dữ liệu ra màn hình ( Speech App )
function SpeechlyApp({ exercise, cancleMic, courseId }: IAudioHandleProps) {
  const [form] = Form.useForm();
  const [answer, setAnswer] = React.useState<string>(' ');
  const { segment } = useSpeechContext();
  // Xử lý khi người dùng change value bằng click event
  const changeValueRadio = (e) => {
    setAnswer(e.target.value);
  };

  //   Xử lý khi kết thúc nhận dữ liệu từ Mic
  //   Set answer bằng dữ liệu nhận được từ mic, hide Mic
  //   Sử dụng useEffect để chạy chỉ khi dữ liệu được nhập từ Mic thay đổi

  React.useEffect(() => {
    if (segment?.isFinal) {
      const ans = segment.words.map((w) => w.value).join(' ');
      cancleMic();
      setAnswer(ans);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [segment]);

  const speak = (word) => {
    var msg = new SpeechSynthesisUtterance();
    msg.text = word;
    window.speechSynthesis.speak(msg);
  };

  const onFinish = (values: any) => {
    let id_answer;
    // eslint-disable-next-line
    exercise.answer.map((item) => {
      if (item.name === values.ans) {
        id_answer = item._id;
        return id_answer;
      }
    });
    const qs = {
      id_course: courseId,
      id_answer: id_answer,
    };
    checkAnswerApi(qs).then((response) => {
      const { data } = response;
      if (data.data) {
        message.success('Bạn đã trả lời đúng !');
      } else {
        message.error('Bạn đã trả lời sai !');
        // let newEx = exercise.sort(function () {
        //   return 0.5 - Math.random();
        // });
      }
      form.resetFields();
    });
  };

  //   Return về view của exercise

  return (
    <div>
      <div>
        <Typography.Text>
          <Button onClick={() => speak(exercise?.question)}>
            <CustomerServiceOutlined />
          </Button>{' '}
          {exercise?.question}
        </Typography.Text>
        <hr />
      </div>
      <div>
        <Form onFinish={onFinish} form={form}>
          <Form.Item name="ans">
            <Radio.Group onChange={changeValueRadio} value={answer}>
              <Space direction="vertical">
                {exercise?.answer?.map((item, index) => (
                  <Radio key={index} value={item.name}>
                    <Button onClick={() => speak(item?.name)}>
                      <CustomerServiceOutlined />
                    </Button>{' '}
                    {item.name}
                  </Radio>
                ))}
              </Space>
            </Radio.Group>
          </Form.Item>
          <Form.Item>
            <Button type="primary" htmlType="submit">
              Submit
            </Button>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
}

// Component lấy dữ liệu từ Mic

const AudioHandle = ({ exercise, visibleMic, cancleMic, courseId }: IAudioHandleProps) => {
  return (
    <SpeechProvider appId="682f6e25-d227-4491-ad95-a30226aa20c2" language="en-US">
      <BigTranscriptContainer>
        <BigTranscript />
      </BigTranscriptContainer>
      {/* Nếu visible = true thì hiển trị ra Mic */}
      {visibleMic ? (
        <PushToTalkButtonContainer>
          <PushToTalkButton captureKey=" " />
          <ErrorPanel />
        </PushToTalkButtonContainer>
      ) : null}
      <SpeechlyApp
        exercise={exercise}
        visibleMic={visibleMic}
        cancleMic={cancleMic}
        courseId={courseId}
      />
    </SpeechProvider>
  );
};

export default React.memo(AudioHandle);
