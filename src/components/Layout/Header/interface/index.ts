export interface IloginModalProps {
  isVisible: boolean;
  onCancel: () => void;
}

export interface IregisterModalProps {
  isVisible: boolean;
  onCancel: () => void;
}

export interface IloginFormValues {
  email: string;
  password: string;
}
