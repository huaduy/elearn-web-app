import { registerAction } from 'actions/auth';
import { IregisterPayload } from 'actions/interface';
import { Modal, Form, Input, Button, message } from 'antd';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { IregisterModalProps } from '../../interface';

const RegisterModal = ({ onCancel, isVisible }: IregisterModalProps) => {
  const [form] = Form.useForm();
  const dispatch = useDispatch();
  const handleOk = () => {};
  const onFinish = async (values: IregisterPayload) => {
    dispatch(registerAction(values));
  };
  const renderTitle = () => {
    return <div className="md-title-login">Đăng Kí</div>;
  };

  const { register }: any = useSelector<any>((state) => state.auth);

  useEffect(() => {
    if (register) {
      message.success(
        'Đăng ký tài khoản thành công! Vui lòng kiểm tra email để xác thực tài khoản'
      );
      onCancel();
      form.resetFields();
    }
    //eslint-disable-next-line
  }, [register]);

  return (
    <div className="modal modal-register">
      <Modal
        title={renderTitle()}
        visible={isVisible}
        onOk={handleOk}
        onCancel={onCancel}
        centered
        footer={null}
        width={800}
      >
        <Form
          name="basic"
          form={form}
          className="form-register"
          labelCol={{ span: 24 }}
          wrapperCol={{ span: 24 }}
          initialValues={{ remember: true }}
          onFinish={onFinish}
          layout={'vertical'}
          autoComplete="off"
        >
          <Form.Item
            label="Họ và tên"
            name="name"
            className="label-login"
            rules={[{ required: true, message: 'Bạn chưa nhập họ và tên!' }]}
          >
            <Input className="input-login" />
          </Form.Item>

          <Form.Item
            label="Email"
            name="email"
            className="label-login"
            rules={[{ required: true, message: 'Bạn chưa nhập Email!' }]}
          >
            <Input className="input-login" />
          </Form.Item>

          <Form.Item
            label="Mật khẩu"
            className="label-login"
            name="password"
            rules={[{ required: true, message: 'Bạn chưa nhập mật khẩu!' }]}
          >
            <Input.Password className="input-login" />
          </Form.Item>

          <Form.Item
            label="Xác nhận mật khẩu"
            className="label-login"
            name="re-password"
            rules={[{ required: true, message: 'Bạn chưa xác nhận mật khẩu!' }]}
          >
            <Input.Password className="input-login" />
          </Form.Item>

          <Form.Item wrapperCol={{ offset: 0, span: 24 }}>
            <Button className="btn-login" type="primary" htmlType="submit">
              Đăng Kí
            </Button>
          </Form.Item>
        </Form>
      </Modal>
    </div>
  );
};

export default React.memo(RegisterModal);
