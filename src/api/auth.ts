import API from './axiosClient';
import { IqueryLogin, IqueryRegister } from './interface';

// Auth API

const BASE_USER_URL = '/user';

export const loginApi = (user: IqueryLogin) => API.post(`${BASE_USER_URL}/login`, user);
export const registerApi = (user: IqueryRegister) => API.post(`${BASE_USER_URL}/register`, user);
export const verifyEmailApi = (id) => API.post(`${BASE_USER_URL}/verify-email`, id);
export const getMyclassApi = () => API.post(`${BASE_USER_URL}/show-course`);

// Profile API

export const getProfileApi = async () => API.post(`${BASE_USER_URL}/profile`);
