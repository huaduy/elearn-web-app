import API from './axiosClient';

// Viết API

const BASE_USER_URL = '/user';

export const getCourseApi = async (qs) => API.post(`${BASE_USER_URL}/detail-course`, qs);

export const getLessonApi = async (qs) => {
  return API.post(`${BASE_USER_URL}/get-lesson`, qs);
};

export const checkAnswerApi = async (qs) => {
  return API.post(`${BASE_USER_URL}/check-correct-answer`, qs);
};
