export interface IqueryLogin {
  email: string;
  password: string;
}

export interface IqueryRegister {
  name: string;
  email: string;
  password: string;
}

export interface IqueryVerifyEmail {
  id: string;
}
