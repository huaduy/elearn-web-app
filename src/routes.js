import Communicate from 'pages/Communicate';
import Home from 'pages/Home';
import Course from 'pages/Course';
import Personal from 'pages/Personal';
import ListCourse from 'pages/ListCourse';
import VerifyEmail from 'pages/VerifyEmail';

const routes = [
  {
    path: '/communicate',
    component: Communicate,
    layout: true,
  },
  {
    path: '/learn/:id',
    component: Course,
    layout: true,
  },
  {
    path: '/personal',
    component: Personal,
    layout: true,
  },
  {
    path: '/courses/list',
    component: ListCourse,
    layout: true,
  },
  {
    path: '/verify-email/:id',
    component: VerifyEmail,
    layout: false,
  },
  {
    path: '/',
    component: Home,
    layout: true,
  },
];

export default routes;
